package com.PageObjects;

import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.time.temporal.ChronoUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TAP_Properties extends PageObject {

    @FindBy(xpath = "/html/body/div/div/nav/div/ul[1]/a[2]")
    public WebElement propertiesPage;
    @FindBy(name = "BrandInput")
    public WebElement brandSelect;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[1]/div[1]/div/select/option[2]")
    public WebElement brandOption;
    @FindBy(name = "MarketInput")
    public WebElement marketSelect;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[1]/div[2]/div/select/option[5]")
    public WebElement marketOption;
    @FindBy(name = "SFCCInstanceInput")
    public WebElement sfccSelect;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[1]/div[3]/div/select/option[2]")
    public WebElement sfccOption;
    @FindBy(name = "OMNIInstanceInput")
    public WebElement omniSelect;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[1]/div[4]/div/select/option[2]")
    public WebElement omniOption;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div/div/ul/li[1]/a")
    public WebElement dropdown;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[3]/label")
    public WebElement urlOpt;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[3]/div/div[1]/input")
    public WebElement urlInput;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[4]/label")
    public WebElement marketOpt;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[4]/div/div[1]/input")
    public WebElement marketInput;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[5]/label")
    public WebElement apparelOpt;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[5]/div/div[1]/input")
    public WebElement apparelInput;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[6]/label")
    public WebElement apparelSizeOpt;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[6]/div/div[1]/input")
    public WebElement apparelSizeInput;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[7]/label")
    public WebElement quantityOpt;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[7]/div/div[1]/input")
    public WebElement quantityValue;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[8]/label")
    public WebElement deliveryOpt;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[8]/div/div[1]/input")
    public WebElement deliveryValue;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[9]/label")
    public WebElement giftCardOpt;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[9]/div/div[1]/input")
    public WebElement giftCardValue;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[10]/label")
    public WebElement giftCardPinOpt;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[10]/div/div[1]/input")
    public WebElement giftCardPinValue;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[11]/label")
    public WebElement brandOpt;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[3]/div/form/div[11]/div/div[1]/input")
    public WebElement brandValue;

    Logger LOGGER = Logger.getLogger(TAP_Properties.class.getName());

    public void changeToProperties() {
        propertiesPage.click();
    }

    public void completeSelects() {
        // Complete Brand select
        brandSelect.click();
        brandOption.click();
        // Complete Market select
        marketSelect.click();
        marketOption.click();
        // Complete SFCC Instance select
        sfccSelect.click();
        sfccOption.click();
        // Complete OMNI Instance select
        omniSelect.click();
        omniOption.click();
    }

    public void waitLoading() {
        LOGGER.log(Level.INFO, "Waiting for the spinner to disappear...");
        try {
            withTimeoutOf(5, ChronoUnit.SECONDS).waitForPresenceOf(By.xpath("/html/body/div/div/div[1]/div/div/div[3]/div[1]/div"));
        }catch(TimeoutException ex) {
            LOGGER.log(Level.INFO, "Spinner wasn't present when checking...", ex);
        } finally {
            withTimeoutOf(60, ChronoUnit.SECONDS).waitForElementsToDisappear(By.xpath("/html/body/div/div/div[1]/div/div/div[3]/div[1]/div"));
        }
    }

    public void completeData(String url, String market, String inline_apparel_1, String inline_apparel_1_size, String quantity_1, String delivery, String giftcard_1, String giftcard_1_pin, String brand) {
        // Wait for loading
        waitLoading();
        // Complete URL
        urlOpt.click();
        urlInput.sendKeys(Keys.CONTROL + "a");
        urlInput.sendKeys(Keys.DELETE);
        urlInput.sendKeys(url);
        dropdown.click();
        // Complete Market
        marketOpt.click();
        marketInput.sendKeys(Keys.CONTROL + "a");
        marketInput.sendKeys(Keys.DELETE);
        marketInput.sendKeys(market);
        dropdown.click();
        // Complete Inline_Apparel_1
        apparelOpt.click();
        apparelInput.sendKeys(Keys.CONTROL + "a");
        apparelInput.sendKeys(Keys.DELETE);
        apparelInput.sendKeys(inline_apparel_1);
        dropdown.click();
        // Complete Inline_Apparel_1_size
        apparelSizeOpt.click();
        apparelSizeInput.sendKeys(Keys.CONTROL + "a");
        apparelSizeInput.sendKeys(Keys.DELETE);
        apparelSizeInput.sendKeys(inline_apparel_1_size);
        dropdown.click();
        // Complete Quantity
        quantityOpt.click();
        quantityValue.sendKeys(Keys.CONTROL + "a");
        quantityValue.sendKeys(Keys.DELETE);
        quantityValue.sendKeys(quantity_1);
        dropdown.click();
        // Complete Delivery
        deliveryOpt.click();
        deliveryValue.sendKeys(Keys.CONTROL + "a");
        deliveryValue.sendKeys(Keys.DELETE);
        deliveryValue.sendKeys(delivery);
        dropdown.click();
        // Complete GiftCard
        giftCardOpt.click();
        giftCardValue.sendKeys(Keys.CONTROL + "a");
        giftCardValue.sendKeys(Keys.DELETE);
        giftCardValue.sendKeys(giftcard_1);
        dropdown.click();
        // Complete GiftCard_pin
        giftCardPinOpt.click();
        giftCardPinValue.sendKeys(Keys.CONTROL + "a");
        giftCardPinValue.sendKeys(Keys.DELETE);
        giftCardPinValue.sendKeys(giftcard_1_pin);
        dropdown.click();
        // Complete Brand
        brandOpt.click();
        brandValue.sendKeys(Keys.CONTROL + "a");
        brandValue.sendKeys(Keys.DELETE);
        brandValue.sendKeys(brand);
        dropdown.click();
    }
}
