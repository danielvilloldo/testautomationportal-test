package com.PageObjects;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.concurrent.TimeUnit;

import static java.time.temporal.ChronoUnit.SECONDS;

@DefaultUrl("http://localhost:3000/test-automation-portal")

public class TAP_Scenario extends PageObject {

    @FindBy(xpath = "/html/body/div/div/div[1]/div/div/div[1]/div[1]/button")
    public WebElement addScenerioBtn;
    @FindBy(id = "EcomAddBtn-0")
    public WebElement eComCard;
    @FindBy(id = "OmniAddBtn-0")
    public WebElement omniHubCard;

    public TAP_Scenario(WebDriver driver) {
        super (driver);
    }

    public void openPage() {
        this.setImplicitTimeout(15, SECONDS);
        this.getDriver().manage().window().maximize();
        this.open();
        waitLoad();
    }

    public void waitLoad() {
        this.getDriver().manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
    }

    public void addNewScenario() {
        addScenerioBtn.click();
    }

    public void chooseScenario() {
        this.setImplicitTimeout(5, SECONDS);
        if(eComCard.isDisplayed() &&  omniHubCard.isDisplayed()) {
            eComCard.click();
            omniHubCard.click();
        }
    }
}
