package com.PageObjects;

import net.thucydides.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TAP_Trigger extends PageObject {

    @FindBy(xpath = "/html/body/div/div/nav/div/ul[1]/a[3]")
    public WebElement triggerPage;
    @FindBy(xpath = "/html/body/div/div/div[1]/div/div[1]/button[1]")
    public WebElement executeBtn;
    @FindBy (xpath = "/html/body/div/div/div[3]/div/div/div[1]")
    public WebElement confirmationAlert;

    public void changeToTrigger() {
        triggerPage.click();
    }

    public void executeTest() {
        executeBtn.click();
    }

    public void verifyTest() {
        Assert.assertEquals("Execution Triggered!", confirmationAlert.getText());
    }


}
