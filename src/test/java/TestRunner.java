import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)

@CucumberOptions(
        features = {"src/test/resources/features/TestAutomationPortal.feature"},
        glue = {"com/TestAutomationPortal_FESteps"}
)

public class TestRunner {

}
