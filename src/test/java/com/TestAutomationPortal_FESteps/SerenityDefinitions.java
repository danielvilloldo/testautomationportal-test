package com.TestAutomationPortal_FESteps;

import com.TestAutomationPortal_FESteps.SerenitySteps.SerenitySteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class SerenityDefinitions {

    @Steps
    SerenitySteps serenitySteps;

    @Given("^the microservice opened$")
    public void openMicroservice() {
        serenitySteps.openMicroservice();
    }

    @When("^click 'Add a new scenario' button$")
    public void addScenario() {
        serenitySteps.addScenario();
    }

    @Then("^choose one scenario for test it$")
    public void chooseScenario() {
        serenitySteps.setScenario();
    }

    @Then("^change to the Properties screen to complete the data$")
    public void changeToProperties() {
        serenitySteps.changeToProperties();
    }

    @And("^complete the brand, market, SFCC Instance and OMNI Instance$")
    public void completeSelects() {
        serenitySteps.completeSelects();
    }

    @And("^complete url with (.*), market with (.*), inline_apparel_1 with (.*), inline_apparel_1_size with (.*), quantity_1 with (.*), delivery with (.*), giftcard_1 with (.*), giftcard_1_pin with (.*) and brand with (.*)$")
    public void completeData(String url, String market, String inline_apparel_1, String inline_apparel_1_size, String quantity_1, String delivery, String giftcard_1, String giftcard_1_pin, String brand) {
        serenitySteps.completeData("master.glass.05ri3.k8s.asgard.dub.aws.k8s.3stripes.net", "FR", "BA7261", "3.5", "1", "Zinedine,Zidane,Avenue Simon Bolivar 46,,Paris,75019,11123123123,checkout_glass@chkauto.com,y,y,y", "5045075745499900330", "0197", "adidas");
    }

    @Then("^change to the Confirm & Trigger screen$")
    public void changeToTrigger() {
        serenitySteps.changeToTrigger();
    }

    @And("^click in the Trigger Test Automation button$")
    public void executeTest() {
        serenitySteps.executeTest();
    }

    @Then("^verify that the test was right$")
    public void verifyTest() {
        serenitySteps.verifyTest();
    }

}
