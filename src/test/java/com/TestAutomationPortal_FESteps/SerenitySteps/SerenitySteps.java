package com.TestAutomationPortal_FESteps.SerenitySteps;

import com.PageObjects.TAP_Properties;
import com.PageObjects.TAP_Scenario;
import com.PageObjects.TAP_Trigger;
import net.thucydides.core.annotations.Step;

public class SerenitySteps {

    private TAP_Scenario tap_scenario;
    private TAP_Properties tap_properties;
    private TAP_Trigger tap_trigger;

    @Step("Given the microservice opened")
    public void openMicroservice() {
        tap_scenario.openPage();
    }

    @Step("When click 'Add a new scenario' button")
    public void addScenario() {
        tap_scenario.addNewScenario();
    }

    @Step("Then choose one scenario for test it")
    public void setScenario() {
        tap_scenario.chooseScenario();
    }

    @Step("Then change to the Properties screen to complete the data")
    public void changeToProperties() {
        tap_properties.changeToProperties();
    }

    @Step("And complete the brand, market, SFCC Instance and OMNI Instance")
    public void completeSelects() {
        tap_properties.completeSelects();
    }

    @Step("And complete url with (.*), market with  (.*), inline_apparel_1 with  (.*), inline_apparel_1_size with  (.*), quantity_1 with  (.*), delivery with  (.*), giftcard_1 with  (.*), giftcard_1_pin with  (.*) and brand with  (.*)")
    public void completeData(String url, String market, String inline_apparel_1, String inline_apparel_1_size, String quantity_1, String delivery, String giftcard_1, String giftcard_1_pin, String brand) {
        tap_properties.completeData("master.glass.05ri3.k8s.asgard.dub.aws.k8s.3stripes.net", "FR", "BA7261", "3.5", "1", "Zinedine,Zidane,Avenue Simon Bolivar 46,,Paris,75019,11123123123,checkout_glass@chkauto.com,y,y,y", "5045075745499900330", "0197", "adidas");
    }

    @Step("Then change to the Confirm & Trigger screen")
    public void changeToTrigger() {
        tap_trigger.changeToTrigger();
    }

    @Step("And click in the Trigger Test Automation button")
    public void executeTest() {
        tap_trigger.executeTest();
    }

    @Step("Then verify that the test was right")
    public void verifyTest() {
        tap_trigger.verifyTest();
    }

}
