Feature: Complete a test choosing a scenario and some properties.

  Scenario Outline: Complete a test in the Test Automation Portal website, choosing a scenario and some properties
    Given the microservice opened
    When click 'Add a new scenario' button
    Then choose one scenario for test it
    Then change to the Properties screen to complete the data
    And complete the brand, market, SFCC Instance and OMNI Instance
    And complete url with <url>, market with <market>, inline_apparel_1 with <inline_apparel_1>, inline_apparel_1_size with <inline_apparel_1_size>, quantity_1 with <quantity_1>, delivery with <delivery>, giftcard_1 with <giftcard_1>, giftcard_1_pin with <giftcard_1_pin> and brand with <brand>
    Then change to the Confirm & Trigger screen
    And click in the Trigger Test Automation button
    Then verify that the test was right

    Examples:
      | url                                                    | market | inline_apparel_1 | inline_apparel_1_size | quantity_1 | delivery                                                                                          | giftcard_1          | giftcard_1_pin | brand  |
      | master.glass.05ri3.k8s.asgard.dub.aws.k8s.3stripes.net | FR     | BA7261           | 3.5                   | 1          | Zinedine,Zidane,Avenue Simon Bolivar 46,,Paris,75019,11123123123,checkout_glass@chkauto.com,y,y,y | 5045075745499900330 | 0197           | adidas |